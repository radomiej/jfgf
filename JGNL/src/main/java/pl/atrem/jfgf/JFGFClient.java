package pl.atrem.jfgf;

import pl.atrem.jfgf.client.connection.RemoteClient;

public class JFGFClient
{

	public static RemoteClient createHostClient(String host, int tcp, int udp) {

		return RemoteClient.createClient(host, tcp, udp);

	}
}
