package pl.atrem.jfgf;

public interface RegisterObjects {
	
	@SuppressWarnings("rawtypes")
	public Class[] getRegisterClasses();
}
