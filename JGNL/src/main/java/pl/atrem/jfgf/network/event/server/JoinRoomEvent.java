package pl.atrem.jfgf.network.event.server;

import pl.atrem.jfgf.network.event.Event;


public class JoinRoomEvent implements Event
{
	private int roomId;

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
}
