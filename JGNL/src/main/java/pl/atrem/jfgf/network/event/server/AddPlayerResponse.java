package pl.atrem.jfgf.network.event.server;

import pl.atrem.jfgf.network.event.Response;


public class AddPlayerResponse implements Response
{
	public AddPlayerResponse() {
		
	}
	public AddPlayerResponse(boolean succes) {
		this.succes = succes;
	}
	public boolean succes;
	public int currentRoomId;
}
