package pl.atrem.jfgf.network;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pl.atrem.jfgf.network.event.EventStruct;
import pl.atrem.jfgf.network.event.error.ErrorResponse;
import pl.atrem.jfgf.network.event.server.AddPlayerEvent;
import pl.atrem.jfgf.network.event.server.AddPlayerResponse;
import pl.atrem.jfgf.network.event.server.CreateRoomEvent;
import pl.atrem.jfgf.network.event.server.CreateRoomResponse;
import pl.atrem.jfgf.network.event.server.CurrentRoomEvent;
import pl.atrem.jfgf.network.event.server.CurrentRoomResponse;
import pl.atrem.jfgf.network.event.server.GetRoomInfoEvent;
import pl.atrem.jfgf.network.event.server.GetRoomInfoResponse;
import pl.atrem.jfgf.network.event.server.JoinRoomEvent;
import pl.atrem.jfgf.network.event.server.JoinRoomResponse;
import pl.atrem.jfgf.network.event.server.RoomListEvent;
import pl.atrem.jfgf.network.event.server.RoomListResponse;
import pl.atrem.jfgf.serwer.RoomInfo;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

public class Network
{	
	public final static int TIMEOUT = 1000;	
	public final static int WRITE_BUFFER = 256 * 1024;	
	public final static int READ_BUFFER = 256 * 1024;
	public static int portTCP = 56555;
	public static int portUDP = 56777;
	
	@SuppressWarnings("rawtypes")
	static private List<Class> rergisterClasses = new ArrayList<>();


	@SuppressWarnings("rawtypes")
	public static void registerClasses(Class[] classToRegister) {
		for(Class c : classToRegister){
			rergisterClasses.add(c);
		}		
	}

	// This registers objects that are going to be sent over the network.
	 @SuppressWarnings("rawtypes")
	public static void register(EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		
		for(Class c : rergisterClasses){
			kryo.register(c);
		}	
		
		//Native class
		kryo.register(char[].class);
		kryo.register(List.class);
		kryo.register(LinkedList.class);
		kryo.register(ArrayList.class);				
		
		//Eventy
		kryo.register(EventStruct.class);
		kryo.register(AddPlayerEvent.class);
		kryo.register(AddPlayerResponse.class);		
		kryo.register(CreateRoomEvent.class);
		kryo.register(CreateRoomResponse.class);
		kryo.register(JoinRoomEvent.class);
		kryo.register(JoinRoomResponse.class);
		kryo.register(RoomInfo.class);
		kryo.register(RoomListEvent.class);
		kryo.register(RoomListResponse.class);		
		kryo.register(CurrentRoomEvent.class);
		kryo.register(CurrentRoomResponse.class);
		kryo.register(GetRoomInfoEvent.class);
		kryo.register(GetRoomInfoResponse.class);
		kryo.register(ErrorResponse.class);		
		
	}

	public static void setPorts(int portTcp, int portUdp) {
		portTCP = portTcp;
		portUDP = portUdp;
	}
	
}
