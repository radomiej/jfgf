package pl.atrem.jfgf.network;

import pl.atrem.jfgf.network.event.Event;


public interface Sender
{
	public void send(Event event);	

	public int getID();	
}
