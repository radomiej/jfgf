package pl.atrem.jfgf.network.event.error;

import pl.atrem.jfgf.network.event.Response;



public class ErrorResponse implements Response
{
	public String error;
	
	public ErrorResponse(){
		
	}
	
	public ErrorResponse(String messages){
		error = messages;
	}
}
