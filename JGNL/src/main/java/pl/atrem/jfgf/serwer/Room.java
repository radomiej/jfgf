package pl.atrem.jfgf.serwer;

import pl.atrem.jfgf.network.event.Event;

public interface Room
{	
	public abstract void incomingEvent(Event incoming, PlayerSender sendingPlayer);	

	public int getId();

	public boolean isOpen();
	
	public String getName();	

	public String getGroup();
	
	public Strainer getStrainer();	
	
	void setStrainer(Strainer myStrainer);

}
