package pl.atrem.jfgf.serwer;

import pl.atrem.jfgf.network.Sender;
import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.serwer.connection.RemotePlayer;

public class PlayerSender implements Sender
{

	private RemotePlayer connection;
	private Session session;

	public PlayerSender(RemotePlayer conn) {
		connection = conn;
	}

	@Override
	public void send(Event event) {
		connection.sendTCP(event);
	}

	@Override
	public int getID() {
		return connection.getID();
	}

	public void moveToRoom(String name) throws Exception {
		connection.moveToRoom(name);
	}

	public int getCurrentId() {
		return connection.getCurrentId();
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public RemotePlayer getConnection(){
		return connection;
	}
}
