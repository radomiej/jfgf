package pl.atrem.jfgf.serwer;

import java.util.HashMap;
import java.util.Map;

import pl.atrem.jfgf.network.Sender;
import pl.atrem.jfgf.network.event.Event;



public class EventExecutorMachine
{
	
	private Map<Class<? extends Event>, EventExecutor> behaviors = new HashMap<>();
	
	public void registerEventExecutor(Class<? extends Event> anyEventClass, EventExecutor executor){
		behaviors.put(anyEventClass, executor);		
	}
	
	public synchronized void executeEvent(Event executeEvent, Sender connection){
		EventExecutor executor = behaviors.get(executeEvent.getClass());
		if(executor == null) throw new NullPointerException("Brak zdefiniowanego zachowania dla podanego zdarzenia: " + executeEvent.getClass().getSimpleName());
		executor.invoke(executeEvent, connection);
	}
}
