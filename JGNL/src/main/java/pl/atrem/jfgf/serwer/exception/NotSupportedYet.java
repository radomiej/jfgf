package pl.atrem.jfgf.serwer.exception;

public class NotSupportedYet extends NullPointerException {
	
	public NotSupportedYet(){
		super();
	}
	
	public NotSupportedYet(String msg){
		super(msg);
	}
}
