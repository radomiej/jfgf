package pl.atrem.jfgf.serwer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StrainerMultithreadWorker
{
	static Logger logger = LogManager.getLogger(StrainerMultithreadWorker.class.getName());

	ExecutorService executor;

	public StrainerMultithreadWorker(int countUsageCore) {
		executor = Executors.newFixedThreadPool(countUsageCore);
		
	}

	public void invokeInOwnThread(StrainerImpl strainerImpl) {
		executor.execute(strainerImpl);

	}

}