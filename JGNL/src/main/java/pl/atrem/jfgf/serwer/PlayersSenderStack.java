package pl.atrem.jfgf.serwer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pl.atrem.jfgf.network.Sender;
import pl.atrem.jfgf.network.event.Event;

public class PlayersSenderStack
{

	private List<PlayerSender> remotesPlayers = new LinkedList<PlayerSender>();

	public synchronized void remove(Sender sendingPlayer) {
		remotesPlayers.remove(sendingPlayer);
	}

	public synchronized List<PlayerSender> getList() {
		List<PlayerSender> clonePlayersList = new ArrayList<PlayerSender>();

		for (PlayerSender remotePlayer : remotesPlayers) {
			clonePlayersList.add(remotePlayer);
		}

		return clonePlayersList;
	}

	public synchronized void add(Sender sendingPlayer) {
		remotesPlayers.add((PlayerSender) sendingPlayer);
	}

	public synchronized int size() {
		return remotesPlayers.size();
	}

	public synchronized void sendToAll(Event response) {
		for (PlayerSender remotePlayer : remotesPlayers) {
			remotePlayer.send(response);
		}
	}
}
