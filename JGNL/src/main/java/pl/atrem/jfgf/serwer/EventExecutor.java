package pl.atrem.jfgf.serwer;

import pl.atrem.jfgf.network.Sender;
import pl.atrem.jfgf.network.event.Event;

import com.esotericsoftware.kryonet.Connection;

public interface EventExecutor
{
	public void invoke(Event invokingEvent, Sender connection);
}
