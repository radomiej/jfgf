package pl.atrem.jfgf.serwer.connection;

import pl.atrem.jfgf.network.event.EventStruct;
import pl.atrem.jfgf.network.event.error.PlayerDisconnectedEvent;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.KeepAlive;
import com.esotericsoftware.kryonet.Listener;

public class ServerNetworkListener extends Listener
{
	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof KeepAlive) return;

		sendEvent(connection, object);
	}

	@Override
	public void disconnected(Connection connection) {
		PlayerDisconnectedEvent lastEvent = new PlayerDisconnectedEvent();
		RemotePlayer player = (RemotePlayer) connection;
		EventStruct lastEventStruct = new EventStruct(lastEvent, player.getCurrentId());
		
		sendEvent(connection, lastEventStruct);
	}

	private void sendEvent(Connection connection, Object object) {
		RemotePlayer player = (RemotePlayer) connection;
		player.incomingEvent(object);
	}
}
