package pl.atrem.jfgf;

import pl.atrem.jfgf.network.Network;


public class JFGFNetwork {

	public static void registerClass(RegisterObjects registerObjectImpl) {
		Network.registerClasses(registerObjectImpl.getRegisterClasses());		
	}	
}
