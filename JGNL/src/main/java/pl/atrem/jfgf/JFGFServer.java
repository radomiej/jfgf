package pl.atrem.jfgf;

import pl.atrem.jfgf.serwer.EventSender;
import pl.atrem.jfgf.serwer.Room;
import pl.atrem.jfgf.serwer.RoomsManager;
import pl.atrem.jfgf.serwer.Strainer;
import pl.atrem.jfgf.serwer.StrainerImpl;
import pl.atrem.jfgf.serwer.connection.ServerNetwork;
import pl.atrem.jfgf.serwer.exception.NotSupportedYet;
import pl.atrem.jfgf.serwer.exception.OtherRoomWithTheSameNameExist;

public enum JFGFServer {
	INSTANCE;
	
	
	private RoomsManager roomsManager;
	private ServerNetwork server;
	
	
	private JFGFServer(){
		
	}
	
	/**
	 * Rejestruje główny pokój, do którego trafia każdy nowo podłaczony gracz.
	 * @param catcherRoom
	 */
	public void registerCatcherRoom(Room catcherRoom){
		
		if(roomsManager != null) throw new NotSupportedYet("Nie możesz zarejestrować wicej niż jednego pierwszego pokoju");
		roomsManager = RoomsManager.create(catcherRoom);
	}
	
	public Strainer registerRoom(Room r) throws OtherRoomWithTheSameNameExist{
		if(roomsManager == null) throw new NullPointerException("Najpierw zarejestruj pokój główny. registerCatcherRoom()");
		Strainer str = roomsManager.addRoom(r);		
		return str;
	}
	
	public void start(int tcp, int udp){
		EventSender.INSTANCE.start();
		server = new ServerNetwork();
		server.setManager(roomsManager);		
		server.start(tcp, udp);
	}
}
