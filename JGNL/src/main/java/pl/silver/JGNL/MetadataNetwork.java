package pl.silver.JGNL;

import java.util.List;

public interface MetadataNetwork
{
	public void addMetadata(Class... classSendingOverNetwork);
	List<Class> getMetadata();
}
