package pl.atrem.jfgf.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.esotericsoftware.kryonet.Connection;

import pl.atrem.jfgf.client.connection.ClientNetwork;
import pl.atrem.jfgf.client.connection.InternalEventListener;
import pl.atrem.jfgf.network.event.Event;

public class ClientTest
{
	@Test
	public void testClientNetwork(){
		ClientNetwork clientNetwork = new ClientNetwork();	
		
		assertEquals(-1, clientNetwork.getConnectionId());
		
		clientNetwork.addListener(new InternalEventListener() {
			
			@Override
			public void received(Connection connection, Event object) {
				fail("Nie powinieneś dostać żadnego zdarzenia");
			}
		});
		
		clientNetwork.send(new Event() {
		});
	}
}
