package pl.atrem.jfgf.test;

import org.junit.Test;

import pl.atrem.jfgf.JFGFServer;
import pl.atrem.jfgf.RoomDefaultImpl;
import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.serwer.PlayerSender;
import pl.atrem.jfgf.serwer.Room;
import pl.atrem.jfgf.serwer.RoomsManager;
import pl.atrem.jfgf.serwer.Strainer;
import pl.atrem.jfgf.serwer.exception.NotSupportedYet;
import pl.atrem.jfgf.serwer.exception.OtherRoomWithTheSameNameExist;
import junit.framework.TestCase;

public class RoomsManagerTest extends TestCase
{
	@Test
	public void testRoomsManager() {
		Room room = new RoomDefaultImpl(TestName.TEST_NAME_ONE.toString(), TestGroup.TEST_NAME_ONE.toString()) {

			@Override
			public void incomingEvent(Event incoming, PlayerSender sendingPlayer) {
				throw new NotSupportedYet();
			}
		};

		JFGFServer.INSTANCE.registerCatcherRoom(room);
		assertTrue("Zarejestrowany pokój musi mieć przypisanego Strainera", room.getStrainer() != null);
		assertTrue("Strainer pokoju musi mieć zalinkowany pokój", room.getStrainer().getLinkedRoom() != null);
		assertTrue("Strainer pokoju musi być tym samym pokojem co pokój rejestrowany", room.getStrainer()
				.getLinkedRoom() == room);
		assertTrue("Strainer musi być podlinkowany do roomsManagera", room.getStrainer().getRoomsManager() != null);

		RoomsManager roomsManager = room.getStrainer().getRoomsManager();
		assertEquals("Wielkość liter nie powinna być rozróżniana", 1, roomsManager.getRoomsList(
				TestGroup.TEST_NAME_ONE_WITH_CAMEL.toString()).size());
		assertEquals("Błędna liczba pokoi", 1, roomsManager.getRoomsList(TestGroup.TEST_NAME_ONE.toString()).size());
		assertTrue(
				"Pierwszy pokój musi być tym samym pokojem co pierwszy przekazany jako parametr w registerCatcherRoom()",
				roomsManager.getFirstRoomStrainer() == room.getStrainer());

		// Test dodania pokoi o takiej samej nazwie
		room = new RoomDefaultImpl(TestName.TEST_NAME_ONE.toString(), TestGroup.TEST_NAME_ONE.toString()) {

			@Override
			public void incomingEvent(Event incoming, PlayerSender sendingPlayer) {
				throw new NotSupportedYet();
			}
		};
		
		Strainer testStrainer = null;
		try {
			testStrainer = roomsManager.addRoom(room);
			fail("Powinien zostać wyenerowany wyjątek: " + OtherRoomWithTheSameNameExist.class.getSimpleName());
		} catch (OtherRoomWithTheSameNameExist e) {

		}

		// Test dodania kolejnego pokoju o innej nazwie i tej samej grupie
		room = new RoomDefaultImpl(TestName.TEST_NAME_TWO.toString(), TestGroup.TEST_NAME_ONE.toString()) {

			@Override
			public void incomingEvent(Event incoming, PlayerSender sendingPlayer) {
				throw new NotSupportedYet();
			}
		};

		try {
			testStrainer = roomsManager.addRoom(room);
		} catch (OtherRoomWithTheSameNameExist e) {
			fail("Wyjątek nie powienien wystąpić, ponieważ podano inną nazwe pokoju: " + e.getMessage());
		}
		
		assertTrue("Nazwa pokoju powinna być taka sama jak ta podana w konstruktorze", TestName.TEST_NAME_TWO.toString().equalsIgnoreCase(room.getName()));
		assertTrue("Nazwa grupy powinna być taka sama jak ta podana w konstruktorze", TestGroup.TEST_NAME_ONE.toString().equalsIgnoreCase(room.getGroup()));
		assertTrue("Obiekty powinny być te same", testStrainer == room.getStrainer());
		
		assertEquals("Błędna liczba pokoi", 2, roomsManager.getRoomsList(TestGroup.TEST_NAME_ONE.toString()).size());

		assertTrue("Zwrócony strainer powinien być Strainerem pokoju z któego wzięto Id",
				room.getStrainer() == roomsManager.getStrainerByRoomId(room.getId()));
		assertTrue("Domyślny pokój powienien być otwarty", room.getStrainer() == roomsManager
				.getOpenRoomStrainerByRoomId(room.getId()));

		for (int count = 0; count < 1000; count++) {
			assertTrue("Metoda powinna zwracać różne wartości", RoomsManager.getUniqueId() != RoomsManager
					.getUniqueId());
		}		
	}

	public enum TestName {
		TEST_NAME_ONE("testowy"), TEST_NAME_ONE_WITH_CAMEL("TeStOwY"), TEST_NAME_TWO("testowy1");

		private String name;

		private TestName(String name) {
			this.setName(name);
		}

		@Override
		public String toString() {
			return name;
		}

		private void setName(String name) {
			this.name = name;
		}
	}

	public enum TestGroup {
		TEST_NAME_ONE("testowa"), TEST_NAME_ONE_WITH_CAMEL("TeStOwA"), TEST_NAME_TWO("testowa1");

		private String name;

		private TestGroup(String name) {
			this.setName(name);
		}

		@Override
		public String toString() {
			return name;
		}

		private void setName(String name) {
			this.name = name;
		}
	}
}
