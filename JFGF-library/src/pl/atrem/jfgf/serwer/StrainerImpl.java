package pl.atrem.jfgf.serwer;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.serwer.connection.RemotePlayer;

/**
 * 
 * Threadsafe.
 *
 */
public class StrainerImpl implements Strainer
{
	public static class IncomeEvent
	{
		private RemotePlayer sendingPlayer;
		private Event sendingEvent;

		public IncomeEvent() {

		}

		public IncomeEvent(RemotePlayer sender, Event event) {
			this.sendingPlayer = sender;
			this.sendingEvent = event;
		}

		public RemotePlayer getSender() {
			return sendingPlayer;
		}

		public void setSender(RemotePlayer sender) {
			this.sendingPlayer = sender;
		}

		public Event getSenderEvent() {
			return sendingEvent;
		}

		public void setSenderEvent(Event senderEvent) {
			this.sendingEvent = senderEvent;
		}
	}

	static Logger logger = LogManager.getLogger(StrainerImpl.class.getName());

	private LinkedBlockingDeque<IncomeEvent> events = new LinkedBlockingDeque<>();
	
	private AtomicBoolean isEmptyEventStack = new AtomicBoolean(true);

	public static Strainer create(Room linkedRoom) {
		StrainerImpl strainer = new StrainerImpl(linkedRoom);
		linkedRoom.setStrainer(strainer);

		return strainer;
	}

	private Room linkedRoom;
	private RoomsManager roomManager;

	private StrainerImpl(Room linkedRoom) {
		this.setLinkedRoom(linkedRoom);
	}

	@Override
	public synchronized void run() {
		if (!events.isEmpty()) {
			IncomeEvent incomingEvent = events.pollFirst();
			executeEvent(incomingEvent.getSenderEvent(), incomingEvent.getSender().getSender());
		}

		if(events.isEmpty()) isEmptyEventStack.set(true);
		else isEmptyEventStack.set(false);
	}

	@Override
	public void addEvent(Event eventToAdd, RemotePlayer sendingPlayer) {		
		events.push(new IncomeEvent(sendingPlayer, eventToAdd));
		roomManager.getStrainersWorker().invokeInOwnThread(this);
		logger.debug("Dodano zdarzenie do wykonania: " + eventToAdd.getClass().getSimpleName());
	}

	@Override
	public void addEvent(Event eventToAdd, PlayerSender sendingPlayer) {
		addEvent(eventToAdd, sendingPlayer.getConnection());		
	}

	private synchronized void executeEvent(Event event, PlayerSender sendingPlayer) {
		logger.debug("Przekazuje do pokoju zdarzenie: " + event.getClass().getSimpleName());
		getLinkedRoom().incomingEvent(event, sendingPlayer);
	}

	@Override
	public Room getLinkedRoom() {
		return linkedRoom;
	}

	@Override
	public void setLinkedRoom(Room linkedRoom) {
		this.linkedRoom = linkedRoom;
	}

	@Override
	public RoomsManager getRoomsManager() {
		return roomManager;
	}

	@Override
	public void setRoomManager(RoomsManager roomManager) {
		this.roomManager = roomManager;
	}
	
	@Override
	public boolean isEmptyEventStack() {
		return isEmptyEventStack.get();
	}	

}
