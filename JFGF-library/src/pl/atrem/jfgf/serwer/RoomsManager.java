package pl.atrem.jfgf.serwer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;

import pl.atrem.jfgf.network.event.server.RegisterRoomEvent;
import pl.atrem.jfgf.serwer.connection.RemotePlayer;
import pl.atrem.jfgf.serwer.exception.NotFindAnyResult;
import pl.atrem.jfgf.serwer.exception.OtherRoomWithTheSameNameExist;

public class RoomsManager
{
	private static int uniqueId = 0;

	public static int getUniqueId() {
		return uniqueId++;
	}

	public static RoomsManager create(Room hallRoom) {
		RoomsManager newInstance = new RoomsManager(hallRoom);
		hallRoom.getStrainer().setRoomManager(newInstance);
		return newInstance;
	}

	public static RoomsManager create(Room hallRoom, int roomsThread) {
		RoomsManager newInstance = new RoomsManager(hallRoom);
		hallRoom.getStrainer().setRoomManager(newInstance);
		return newInstance;
	}

	private List<Strainer> strainers = new ArrayList<>();
	private Strainer firstRoom;

	private StrainerMultithreadWorker strainersWorker;

	private RoomsManager(Room hallRoom, int workersThread) {
		try {
			setFirstRoom(addRoom(hallRoom));
		} catch (OtherRoomWithTheSameNameExist e) {
			throw new RuntimeException(
					"Nie powinien istnieć żaden pokój przed utworzeniem i zarejestrowaniem pierwszego pokoju", e);
		}
		
		setStrainersWorker(new StrainerMultithreadWorker(workersThread));
	}

	private RoomsManager(Room hallRoom) {
		this(hallRoom, 1);
	}

	private void setFirstRoom(Strainer firstRoom) {
		this.firstRoom = firstRoom;
	}

	public Strainer getFirstRoomStrainer() {
		return firstRoom;
	}

	public synchronized Strainer addRoom(Room roomToAdd) throws OtherRoomWithTheSameNameExist {

		Strainer newStrainer = null;

		for (Strainer existingStrainer : strainers) {
			if (existingStrainer.getLinkedRoom().getName().equalsIgnoreCase(roomToAdd.getName())) throw new OtherRoomWithTheSameNameExist(
					roomToAdd.getName());
		}
		newStrainer = StrainerImpl.create(roomToAdd);
		newStrainer.setRoomManager(this);
		strainers.add(newStrainer);
		
		RegisterRoomEvent registerEvent = new RegisterRoomEvent();
		RemotePlayer emptyPlayer = null;
		newStrainer.addEvent(registerEvent, emptyPlayer);
		
		return newStrainer;
	}

	public synchronized List<Room> getRoomsList(String categoryName) {
		List<Room> response = new LinkedList<Room>();

		for (Strainer r : strainers) {
			if (r.getLinkedRoom().getGroup().equalsIgnoreCase(categoryName)) {
				Room categoryRoom = r.getLinkedRoom();
				response.add(categoryRoom);
			}
		}

		return response;
	}

	public synchronized Strainer getOpenRoomStrainerByRoomId(int roomId) {

		for (Strainer r : strainers) {
			Room waitRoom = (Room) r.getLinkedRoom();
			if (waitRoom.isOpen() && waitRoom.getId() == roomId) { return r; }
		}

		return null;
	}

	public synchronized Strainer getStrainerByRoomName(String criteriaName) throws NotFindAnyResult {

		for (Strainer r : strainers) {
			if (r.getLinkedRoom().getName().equalsIgnoreCase(criteriaName)) { return r; }
		}

		throw new NotFindAnyResult("Nie znaleziono pokoju o podanej nazwie");
	}

	public synchronized Strainer getStrainerByRoomId(int roomId) {

		for (Strainer r : strainers) {
			if (r.getLinkedRoom().getId() == roomId) { return r; }
		}

		throw new NotFindAnyResult("Nie znaleziono pokoju o podanym id");
	}

	public synchronized void removeRoom(Room roomToRemove) {
		strainers.remove(roomToRemove.getStrainer());
	}

	StrainerMultithreadWorker getStrainersWorker() {
		return strainersWorker;
	}

	void setStrainersWorker(StrainerMultithreadWorker strainersWorker) {
		this.strainersWorker = strainersWorker;
	}
}
