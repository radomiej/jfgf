package pl.atrem.jfgf.serwer;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.serwer.connection.RemotePlayer;

public enum EventSender implements Runnable
{
	INSTANCE;

	private boolean work = true;
	Queue<EventToSend> eventList = new ConcurrentLinkedQueue<EventToSend>();
	
	private EventSender() {
				
	}
	
	public void start(){
		Thread th = new Thread(this);
		th.setDaemon(true);
		th.setName(this.getClass().getSimpleName());
		th.start();
	}
	
	public void sendEvent(Event event, RemotePlayer sendingPlayer){
		eventList.add(new EventToSend(event, sendingPlayer));
	}
	
	@Override
	public void run() {
		while(work){
			if(!eventList.isEmpty()){				
				EventToSend sender = eventList.poll();				
				sender.sendingPlayer.sendTCP(sender.event);
				
			}
		}
	}
	
	
	private static class EventToSend{
		public Event event;
		public RemotePlayer sendingPlayer;
		
		EventToSend(Event eventToSend, RemotePlayer sendingPlayer){
			event = eventToSend;
			this.sendingPlayer = sendingPlayer;
		}
	}
}
