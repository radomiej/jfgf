package pl.atrem.jfgf.serwer.connection;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.atrem.jfgf.network.Network;
import pl.atrem.jfgf.network.event.server.AddPlayerEvent;
import pl.atrem.jfgf.serwer.RoomsManager;
import pl.atrem.jfgf.serwer.Strainer;
import pl.atrem.jfgf.serwer.exception.NotFindAnyResult;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

public class ServerNetwork
{
	static Logger logger = LogManager.getLogger(ServerNetwork.class.getName());
	Server server;		
	
	RoomsManager manager;	

	public ServerNetwork() {
		//TODO określić na jakiej podstawie załączać poziom logów dla kryonet
		//if (Debug.isEnabled()) Log.set(Log.LEVEL_DEBUG);
		//else Log.set(Log.LEVEL_ERROR);
		
		Log.set(Log.LEVEL_ERROR);		
		
		server = new Server(Network.WRITE_BUFFER, Network.READ_BUFFER) {			
			@Override
			protected Connection newConnection() {
				if (getManager() == null) throw new NullPointerException(
						"Manager powinien byc za�adowany przed startem");

				RemotePlayer newPlayer = new RemotePlayer();
				AddPlayerEvent event = new AddPlayerEvent();
				event.firstAdd = true;
				
				try{
					Strainer hallRoom = getManager().getStrainerByRoomName("HallRoom");						
					newPlayer.moveToRoom(hallRoom);
					
					hallRoom.addEvent(event, newPlayer);
				}
				catch(NotFindAnyResult ex){
					throw new NullPointerException(
							"Brak szukanego obiektu. Upewnij się że istnieje obiekt o podanej nazwie, który będzie służył za pokój zbiorczy dla nowych graczy");
				}
				return newPlayer;
			}
		};
		
		
		Network.register(server);

	}

	public void start(int tcp, int udp) {
		server.start();
		
		server.addListener(new ServerNetworkListener());
		
		try {
			server.bind(tcp, udp);
		} catch (IOException e) {			
			e.printStackTrace();
		}

		
	}		

	RoomsManager getManager() {
		return manager;
	}

	public void setManager(RoomsManager newManager) {
		this.manager = newManager;
	}	

}
