package pl.atrem.jfgf.serwer.connection;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.network.event.EventStruct;
import pl.atrem.jfgf.serwer.EventSender;
import pl.atrem.jfgf.serwer.PlayerSender;
import pl.atrem.jfgf.serwer.Strainer;

import com.esotericsoftware.kryonet.Connection;

public class RemotePlayer extends Connection
{
	static Logger logger = LogManager.getLogger(RemotePlayer.class.getName());
	
	private Strainer eventInput;
	
	private AtomicInteger currentRoomId = new AtomicInteger(-1);
	
	private PlayerSender sender = new PlayerSender(this);

	public void moveToRoom(String string) throws Exception {
		if (eventInput != null) {
			Strainer newStrainer = this.eventInput.getRoomsManager().getStrainerByRoomName(string);

			if (newStrainer != null) {
				eventInput = newStrainer;
				setCurrentId();				
			}
			else {
				throw new Exception("Brak odnalezionego pokoju, podczas wykonywania przejścia.");
			}
		}
		else {
			throw new Exception("Najpierw należy wstrzyknąć pierwszy Strainer");
		}
	}

	public void moveToRoom(Strainer strainer) {
		this.eventInput = strainer;
		setCurrentId();
	}

	private void setCurrentId() {
		currentRoomId.set(eventInput.getLinkedRoom().getId());
	}

	public int getCurrentId() {
		return currentRoomId.get();
	}

	public Strainer getParent() {
		return eventInput;
	}

	public void incomingEvent(Object incoming) {
		EventStruct event = (EventStruct) incoming;
		if (event.destinationRoomId != currentRoomId.get()) {
			logger.debug("Odrzucono zdarzenie: " + event.sendingEvent.getClass().getSimpleName() + ", ponieważ ma inny destinationId niż aktualny pokój w którym znajduje się instancja gracza.");
			return;
		}

		getParent().addEvent(event.sendingEvent, this);
	}
	
	public void send(Event event){
		EventSender.INSTANCE.sendEvent(event, this);
	}
	
	@Override
	public int sendTCP(Object object) {
		if (object instanceof Event) {
			logger.debug("Player id: " + getID() + " Wysłano zdarzenie: " + object.getClass().getSimpleName());
			EventStruct eventStruc = new EventStruct((Event) object, currentRoomId.get());
			return super.sendTCP(eventStruc);
		}

		// TODO sprawdzić i przepuszać tylko wewnętrzne wiadomości kryoneta,
		// reszte zablokować i zwrócić wartość np. 0 lub -1.
		return super.sendTCP(object);
	}

	@Override
	public int sendUDP(Object object) {
		if (object instanceof Event) {
			EventStruct eventStruc = new EventStruct((Event) object, currentRoomId.get());
			return super.sendUDP(eventStruc);
		}

		// TODO sprawdzić i przepuszać tylko wewnętrzne wiadomości kryoneta,
		// reszte zablokować i zwrócić wartość np. 0 lub -1.
		return super.sendUDP(object);
	}

	public PlayerSender getSender() {
		return sender;
	}

	public void setSender(PlayerSender sender) {
		this.sender = sender;
	}
}
