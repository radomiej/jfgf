package pl.atrem.jfgf.serwer.exception;

public class OtherRoomWithTheSameNameExist extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4150694742261338329L;

	public OtherRoomWithTheSameNameExist(String name) {
		super(name);
	}

}
