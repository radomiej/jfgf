package pl.atrem.jfgf.serwer.exception;

public class NotFindAnyResult extends NullPointerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NotFindAnyResult(){
		super();
	}
	
	public NotFindAnyResult(String message){
		super(message);
	}
}
