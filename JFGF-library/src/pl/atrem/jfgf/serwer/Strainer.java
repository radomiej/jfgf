package pl.atrem.jfgf.serwer;

import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.serwer.connection.RemotePlayer;

public interface Strainer extends Runnable
{
	public void addEvent(Event eventToAdd, RemotePlayer sendingPlayer);
	
	public void addEvent(Event incoming, PlayerSender sendingPlayer);
	
	public Room getLinkedRoom();

	public void setLinkedRoom(Room linkedRoom);	

	public RoomsManager getRoomsManager();

	public void setRoomManager(RoomsManager roomManager);

	public boolean isEmptyEventStack();

	
}
