package pl.atrem.jfgf.client.connection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.atrem.jfgf.network.Network;
import pl.atrem.jfgf.network.event.Event;
import pl.atrem.jfgf.network.event.EventStruct;
import pl.atrem.jfgf.network.event.error.DisconnectEvent;
import pl.atrem.jfgf.network.event.server.AddPlayerResponse;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.KeepAlive;
import com.esotericsoftware.kryonet.Listener;

public class ClientNetwork
{
	static Logger logger = LogManager.getLogger(ClientNetwork.class.getName());
	
	
	private Client client;	
	private List<InternalEventListener> clientListeners = new ArrayList<>();


	private int currentRoomId;

	public ClientNetwork() {
		this.client = new Client(Network.WRITE_BUFFER, Network.READ_BUFFER);
		
		Network.register(client);		
		
		client.addListener(new Listener() {			
			
			@Override
			public void received(Connection connection, Object object) {
				logger.trace("Przyszło zdarzenie: " + object.getClass().getSimpleName());
				if(object instanceof KeepAlive) return;					
				serverEvent(connection, (EventStruct) object);	
			}
			
			@Override
			public void disconnected (Connection connection) {
				DisconnectEvent disconnectEvent = new DisconnectEvent();
				EventStruct eventStruct = new EventStruct();
				eventStruct.sendingEvent = disconnectEvent;
				eventStruct.destinationRoomId = getCurrentRoomId();
				serverEvent(connection, eventStruct);	
			}
		});
	}

	public void start(String hostServer, int tcp, int udp) throws IOException {
		client.start();
		try {
			client.connect(Network.TIMEOUT, hostServer, tcp, udp);
		} catch (IOException ex) {
			throw ex;
		}
	}

	private void serverEvent(Connection connection, EventStruct object) {
		
		if(object.sendingEvent instanceof AddPlayerResponse){
			AddPlayerResponse response = (AddPlayerResponse) object.sendingEvent;
			//Przypisanie id aktaulnego pokoju w kt�rym si znajduje gracz
			logger.info("Zmieniono pokój gracza. Stare ID: " + getCurrentRoomId() + " Nowe ID: " + response.currentRoomId);
			if(response.succes) setCurrentRoomId(response.currentRoomId);					
		}			
		else if(object.destinationRoomId != getCurrentRoomId()) return;		
		
		
		for (InternalEventListener listener : clientListeners) {
			listener.received(connection, object.sendingEvent);
		}
	}

	public void addListener(InternalEventListener newListener) {
		clientListeners.add(newListener);
	}

	public void clearListener() {
		if (clientListeners.size() > 1) {
			logger.debug("Wyczyszczono listę listenerów (więcej niż 1)");
		}
		clientListeners.clear();
	}
	public void setListener(InternalEventListener setListener) {
		
		List<InternalEventListener> oldListeners = new LinkedList<InternalEventListener>();
		oldListeners.addAll(clientListeners);
		
		clientListeners.add(setListener);
		
		for(InternalEventListener listenerToRemove : oldListeners){
			clientListeners.remove(listenerToRemove);
		}
		
	}
	public void removeListener(InternalEventListener removeListener) {
		clientListeners.remove(removeListener);
	}

	public void send(Event event) {
		EventStruct eventStruct = new EventStruct(event, getCurrentRoomId());
		client.sendTCP(eventStruct);
	}

	private int getCurrentRoomId() {
		return currentRoomId;
	}

	private void setCurrentRoomId(int currentRoomId) {
		logger.trace("Zmieniono id aktualnego pokoju na: " + currentRoomId);
		this.currentRoomId = currentRoomId;
	}	
	
	public int getConnectionId(){
		return client.getID();
	}
}
