package pl.atrem.jfgf.client.connection;

import pl.atrem.jfgf.network.event.Event;

public interface EventListener
{
	void received(RemoteClient connection, Event object);
}
