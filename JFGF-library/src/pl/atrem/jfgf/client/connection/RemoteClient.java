package pl.atrem.jfgf.client.connection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.atrem.jfgf.network.Sender;
import pl.atrem.jfgf.network.event.Event;

import com.esotericsoftware.kryonet.Connection;

/**
 * ThreadSafe. Jedno zdarzenie naraz.
 * 
 * @author Radomiej
 *
 */
public class RemoteClient implements Sender
{
	static Logger logger = LogManager.getLogger(RemoteClient.class.getName());

	public static RemoteClient createClient(String host, int tcp, int udp) {
		RemoteClient instance = new RemoteClient(host, tcp, udp);
		logger.info("Utworzono klienta zdalnego");
		return instance;
	}

	private ClientNetwork clientNetwork;
	private List<EventListener> listeners = new ArrayList<>();

	private String host;
	private int tcp, udp;

	private boolean connected;	

	private RemoteClient(String host, int tcp, int udp) {

		this.host = host;
		this.tcp = tcp;
		this.udp = udp;

		this.clientNetwork = new ClientNetwork();
		clientNetwork.addListener(new InternalEventListener() {
			@Override
			public void received(Connection connection, Event object) {
				recivedEvent(connection, object);
			}

		});
	}

	public void connect() throws IOException {
		this.clientNetwork.start(host, tcp, udp);
		setConnected(true);
	}

	public void send(Event event) {
		logger.trace("Wysłano zdarzenie na serwer: " + event.getClass().getSimpleName());
		clientNetwork.send(event);
	}

	private synchronized void recivedEvent(Connection connection, Event object) {
		logger.trace("Otrzymano zdarzenie z serwera: " + object.getClass().getSimpleName());
		
		for (EventListener listener : listeners) {
			listener.received(this, object);
		}
	}

	public void addListener(EventListener listener) {
		listeners.add(listener);
	}

	public void clearListeners() {
		listeners.clear();
	}

	public void removeListener(InternalEventListener listenerToRemove) {
		listeners.remove(listenerToRemove);
	}

	public List<EventListener> getListeners() {
		return listeners;
	}

	public void addListeners(List<EventListener> listeners) {
		this.listeners.addAll(listeners);
	}

	public boolean isConnected() {
		return connected;
	}

	private void setConnected(boolean connected) {
		this.connected = connected;
	}

	public void setListener(EventListener setListener) {

		List<EventListener> oldListeners = new LinkedList<EventListener>();
		oldListeners.addAll(listeners);

		listeners.add(setListener);

		for (EventListener listenerToRemove : oldListeners) {
			listeners.remove(listenerToRemove);
		}

	}

	@Override
	public int getID() {
		return clientNetwork.getConnectionId();
	}
}
