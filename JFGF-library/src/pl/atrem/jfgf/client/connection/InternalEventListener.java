package pl.atrem.jfgf.client.connection;

import pl.atrem.jfgf.network.event.Event;

import com.esotericsoftware.kryonet.Connection;

public interface InternalEventListener
{
	void received(Connection connection, Event object);
}
