package pl.atrem.jfgf.network.event.server;

import pl.atrem.jfgf.network.event.Response;





public class CreateRoomResponse implements Response
{
	private boolean succes = false;

	public boolean isSucces() {
		return this.succes;
	}

	public void setSucces(boolean succes) {
		this.succes = succes;
	}
}
