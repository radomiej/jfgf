package pl.atrem.jfgf.network.event.server;

import java.util.LinkedList;
import java.util.List;

import pl.atrem.jfgf.network.event.Response;
import pl.atrem.jfgf.serwer.RoomInfo;

public class RoomListResponse implements Response
{
	List<RoomInfo> rooms = new LinkedList<>();
	public void addRoomStruct(RoomInfo struct) {
		rooms.add(struct);
	}
	public List<RoomInfo> getRooms() {
		return rooms;
	}

}
