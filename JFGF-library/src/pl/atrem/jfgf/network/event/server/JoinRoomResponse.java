package pl.atrem.jfgf.network.event.server;

import pl.atrem.jfgf.network.event.Response;




public class JoinRoomResponse implements Response
{
	private boolean joinSucces;

	public boolean isJoinSucces() {
		return joinSucces;
	}

	public void setJoinSucces(boolean joinSucces) {
		this.joinSucces = joinSucces;
	}
}
