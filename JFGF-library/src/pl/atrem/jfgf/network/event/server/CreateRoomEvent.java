package pl.atrem.jfgf.network.event.server;

import pl.atrem.jfgf.network.event.Event;


public class CreateRoomEvent implements Event
{
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
