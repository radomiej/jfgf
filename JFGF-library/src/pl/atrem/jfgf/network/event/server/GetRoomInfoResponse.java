package pl.atrem.jfgf.network.event.server;

import pl.atrem.jfgf.network.event.Response;
import pl.atrem.jfgf.serwer.RoomInfo;

public class GetRoomInfoResponse implements Response
{
	public RoomInfo roomInfo;
}
