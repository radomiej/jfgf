package pl.atrem.jfgf.network.event;


public class EventStruct {
	
	
	public EventStruct(){
		
	}
	public EventStruct(Event event, int currentRoomId){
		sendingEvent = event;
		destinationRoomId = currentRoomId;
	}	
	
	public Event sendingEvent;
	public int destinationRoomId;
}
