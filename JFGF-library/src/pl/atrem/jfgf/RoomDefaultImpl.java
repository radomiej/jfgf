package pl.atrem.jfgf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.atrem.jfgf.network.Sender;
import pl.atrem.jfgf.network.event.server.AddPlayerResponse;
import pl.atrem.jfgf.serwer.PlayerSender;
import pl.atrem.jfgf.serwer.PlayersSenderStack;
import pl.atrem.jfgf.serwer.Room;
import pl.atrem.jfgf.serwer.RoomsManager;
import pl.atrem.jfgf.serwer.Strainer;
import pl.atrem.jfgf.serwer.connection.RemotePlayer;

public abstract class RoomDefaultImpl implements Room
{
	static Logger logger = LogManager.getLogger(RoomDefaultImpl.class.getName());
	
	private int id = RoomsManager.getUniqueId();
	private String name, group;
	private Strainer myStrainer;
	private boolean open = true;

	protected PlayersSenderStack players = new PlayersSenderStack();

	public RoomDefaultImpl(String name, String group) {
		setGroup(group);
		setName(name);		
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}
	
	private void setName(String roomName) {
		this.name = roomName;
	}
	
	@Override
	public Strainer getStrainer() {
		return myStrainer;
	}
	
	@Override
	public void setStrainer(Strainer myStrainer) {
		this.myStrainer = myStrainer;
	}
	
	@Override
	public boolean isOpen(){
		return this.open;
		
	}

	@Override
	public String getGroup() {
		return group;
	}
	
	private void setGroup(String group) {
		this.group = group;
	}	
	
	protected void movePlayerToThis(Sender sendingPlayer) throws Exception{
		moveToThis((PlayerSender) sendingPlayer, true);	
	}
	
	private void moveToThis(PlayerSender newPlayer, boolean sendResponse) throws Exception{
		players.add(newPlayer);
		try {
			newPlayer.moveToRoom(getName());
			if(sendResponse) sendResponse(newPlayer, true);
		} catch (Exception e) {
			if(sendResponse) sendResponse(newPlayer, false);
			players.remove(newPlayer);
			
			e.printStackTrace();
			throw e;
		}	
	}

	private void sendResponse(PlayerSender newPlayer, boolean succes) {
		
		AddPlayerResponse response = new AddPlayerResponse(succes);
		response.currentRoomId = newPlayer.getCurrentId();
		newPlayer.send(response);
	}
}
